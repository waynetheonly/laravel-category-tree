# Category tree project

This project gives You the ability to create/update/delete categories and then view their
tree structure generate by two different generation methods. Everything can be reached form the admin panel.

**Login information** 

```
Email: root@root
Pass: root
```

**BUT!** Before You try and do that, there are a few steps You need to do.
Please, continue reading.

## Requirements

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension


## Installation

1. Navigate to the root directory of your projects through terminal/command line (`cd /var/www `, `cd C:\xampp\htdocs `
, etc.)

2. Next clone the project `git clone git@bitbucket.org:waynetheonly/category-tree.git`

3. `cd category-tree`

4. `composer install` (`chmod` project recursively if needed)

5. Rename `.env.example` file to `.env`

6. Then run `php artisan key:generate` command in terminal/command line

7. Modify values `DB_DATABASE=`, `DB_USERNAME=` and `DB_PASSWORD=` in `.env` file for DB connection

8. Do not forget to migrate your database with `php artisan migrate`

9. Now the tricky step. For You to be able to login to admin panel you have to put this into your terminal/command line
`php artisan db:seed --class=UsersTableSeeder` **BUT** if you want to have a user AND
demo information for categories, you'd rather want to leave it like this `php artisan db:seed` (don't worry, You can populate 
Your database with demo data with command `php artisan db:seed --class=CategoryTableSeeder` at any time.)

#### And we're done. Now You can navigate to Your project in the browser, login and see the goodness!

### Any feedback is welcome:

- vainius.rutkauskas@gmail.com
- [https://www.linkedin.com/in/vainiusrutkauskas](https://www.linkedin.com/in/vainiusrutkauskas)