<?php

namespace App\Classes;

use App\Category;

class CategoryTree
{
    private $categories;
    private $list;

    public function __construct()
    {
        $this->categories = Category::all()->toArray();
        $this->list = '';
    }

    public function generateRecursive($parent_id = null, $depth = 0)
    {
        if (empty($this->categories)) {
            return;
        }

        if (!isset($parent_id) && is_array($this->categories)) {
            $parent_id = reset($this->categories)['parent_id'];
        }

        foreach ($this->categories as $category) {
            if ($category['parent_id'] === (int)$parent_id) {
                $this->populateTree($category['title'], $depth);
                $this->generateRecursive($category['id'], $depth + 1);
            }
        }
    }

    public function generateIterative()
    {
        if (empty($this->categories)) {
            return;
        }

        $depth = 0;
        $parentStack = array();
        $categories = $this->getFormattedCategories();
        $firstCat = key($categories);

        $parent = $firstCat;

        while (!empty($categories) && ($category = array_shift($categories[$parent])) || ($parent > $firstCat)) {
            if (!empty($category)) {
                if (!empty($categories[$category['id']])) {
                    $this->populateTree($category['title'], $depth);

                    $parentStack[] = $parent;
                    $parent = $category['id'];
                    $depth += 1;
                } else {
                    $this->populateTree($category['title'], $depth);
                }
            } else {
                $depth -= 1;
                $parent = array_pop($parentStack);
            }
        }
    }

    protected function getFormattedCategories()
    {
        $formattedCategories = array();

        foreach ($this->categories as $category) {
            $formattedCategories[$category['parent_id']][] = $category;
        }

        return $formattedCategories;
    }


    public function get()
    {
        return $this->list;
    }

    protected function populateTree($title, $depth)
    {
        $this->list .= sprintf(
            '%s -%s<br/>',
            $this->getTextIndent($depth),
            $title
        );
    }

    protected function getTextIndent($depth)
    {
        return str_repeat('&nbsp;', $depth * 2);
    }
}
