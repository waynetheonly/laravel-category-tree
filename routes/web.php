<?php

Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::resource('categories', 'CategoryController');
Route::get('/tree/{type?}', 'CategoryController@tree')->name('tree');
