$(document).ready(function () {
    $('#modalConfirmDelete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title') // Extract info from data-* attributes
        var url = button.data('url') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-title').text('Confirm deletion');
        modal.find('.modal-body').text('Are you sure you want to delete "'+ title + '"?');
        modal.find('.modal-footer #confirmDelete').attr('action', url);
    });
});
